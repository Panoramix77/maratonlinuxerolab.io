---
layout: page
title: Envíanos tu audio
---
Si quieres, puedes enviarnos tu audio-mensaje grabándolo en el widget de aquí abajo.

Los audios que nos enviéis los publicaremos durante el Maratón. Puedes felicitar a los organizadores y colaboradores, contarnos anécdotas, desearnos mucha suerte... o cualquier cosa que nos quieras contar ;)

<iframe src="https://www.speakpipe.com/widget/inline/yxq7g04nf2lgwbweidm5vtgppoxdzj0c" frameborder="0" width="100%" height="180px"></iframe>

**¡IMPORTANTE! El audio solo puede tener una duración máxima de 90 segundos.**

## Pasos a seguir:

1. Para empezar a grabar pulsa **"Start recording"**.
2. Una vez termines de grabar pulsa **"Stop"**.
3. Escucha tu audio grabado pulsando **"Replay"**.
4. Decide:
  * Si quieres enviar tu audio pulsa **"Send"**.
  * Si no te gusta el audio que has grabado y quieres grabar otro, pulsa **"Reset"**, luego **"Yes"**, ¡y a grabar otro audio!
